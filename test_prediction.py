
from sklearn import datasets
import numpy as np
from machine_learning import train_model


def test_inference_sample():
    model = train_model()
    X, _ = datasets.load_digits(return_X_y=True)
    preds = model.predict(X[0:1])[0]
    assert preds == 0


def test_inference_batch():
    model = train_model()
    X, _ = datasets.load_digits(return_X_y=True)
    preds = model.predict(X[0:100])
    assert np.all(preds < 10)
